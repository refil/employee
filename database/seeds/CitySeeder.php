<?php

use App\City;
use App\State;
use Illuminate\Database\Seeder;

class CitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $arr = array(
            'DUBAI' =>
            array("Dubai"),
            'FUJAIRAH' =>
            array("Fujairah"),
            'ABU DHABI' =>
            array("Abu Dhabi"),
            'ABU DHABI' =>
            array("Abu Dhabi"),
            'AJMAN' =>
            array("Ajman"),
            'UMM AL QUWAIN' =>
            array("Umm Al Quwain"),
            'RAS AL KHAIMAH' =>
            array("Ras Al Khaimah")
        );

        foreach( $arr as $state => $cities ) {
            $state = State::where('name', ucfirst(strtolower($state)))->first();

            foreach($cities as $city) {
                City::create([
                    'state_id'  => $state->id,
                    'name'      => ucfirst(strtolower($city))
                ]);
            }
        }
    }
}
