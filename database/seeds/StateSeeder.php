<?php

use App\Country;
use App\State;
use Illuminate\Database\Seeder;

class StateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $uae_states = array(
            'DXB'	=>	'Dubai',
            'FUJ'	=>	'Fujairah',
            'AUH'	=>	'Abu Dhabi',
            'SHJ'	=>	'Sharjah',
            'AJM'	=>	'Ajman',
            'UMQ'	=>	'Umm Al Quwain',
            'RAK'	=>	'Ras Al Khaimah',
          
        ); 

        $uae = Country::where('name', 'United Arab Emirates')->first();
    

        foreach($uae_states as $short => $state) {  
            State::create([
                'country_id' => $uae->id,
                'name' => $state
            ]);
        }

    }
}
