<?php

use App\Country;
use Illuminate\Database\Seeder;

class CountrySeeder extends Seeder
{
    /*
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $countries = [

            ['name' => 'United Arab Emirates', 'code' =>  'AE'],
        
        ];

        foreach ($countries as $country) {
            Country::create([
                'country_code' =>  $country['code'],
                'name' => $country['name']
            ]);
        }
    }
}
