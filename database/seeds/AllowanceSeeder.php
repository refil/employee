<?php

use App\Allowance;
use Illuminate\Database\Seeder;

class AllowanceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $allowances = [

            ['name' => 'Accommodation'],
            ['name' => 'Transportation'],
            ['name' => 'Living '],
        
        ];

        foreach ($allowances as $allowance) {
            Allowance::create([
                'name' =>  $allowance['name']
            ]);
        }
    }
}
