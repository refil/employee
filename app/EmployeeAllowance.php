<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmployeeAllowance extends Model
{
        /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'employee_allowance';
    protected $guarded = [];
    public function employee()
    {
        return $this->hasMany(Employee::class);
    }
    public function allowance()
    {
        return $this->hasMany(Allowance::class);
    }
}
