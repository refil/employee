<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Response;
use App\Employee;
use App\City;
use App\State;
use App\Country;
use App\Department;
use App\Division;
use App\Allowance;
use App\EmployeeAllowance;


class EmployeeAllowanceManagementController extends Controller
{
      /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // DB::enableQueryLog();
        $employees = DB::table('employees')
        ->join('city', 'employees.city_id', '=', 'city.id')
        ->join('department', 'employees.department_id', '=', 'department.id')
        ->join('state', 'employees.state_id', '=', 'state.id')
        ->join('country', 'employees.country_id', '=', 'country.id')
        ->join('division', 'employees.division_id', '=', 'division.id')
        ->join('employee_allowance', 'employee_allowance.employee_id', '=', 'employees.id')
        ->join('allowances', 'allowances.id', '=', 'employee_allowance.allowance_id')
        ->select('employees.id as emp_id','employees.*','employee_allowance.*', 'allowances.name as allowance_name','department.name as department_name ','division.name as division_name')
        // ->get();
        ->paginate(5);  
        // dd(DB::getQueryLog());
        // dd($employees);
        return view('employees-mgmt/allowance_management', ['employees' => $employees]);
    }
}
